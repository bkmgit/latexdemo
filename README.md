# README

Use GitLab CI to compile an example LaTeX file,
[sample2e.tex](ftp://tug.ctan.org/tex-archive/macros/latex/base/sample2e.tex)

## Generating and downloading

The [LaTeX](https://www.latex-project.org/) file, sample2e.tex 
is automatically compiled to generate a portable document file (pdf) by the 
GitLab continuous integration pipeline upon push of a commit. Go to
https://gitlab.com/bkmgit/latexdemo and check for the green 
tick, circled in the image below, and then click on the download pdf icon, also 
circled.

![Image showing how to download pdf by clicking on the download pdf icon in the top left of the GitLab web interface](./GitLabDownload.png "Download")

If you make a fork of the repository, the pipeline will still run once you 
make a commit, but the badge will not appear. You should be able to download a 
pdf by using this address https://gitlab.com/%{project_path}/-/jobs/artifacts/main/raw/sample2e.pdf?job=compile_pdf
to make your own badge by going to settings and then general as indicated
[here](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/ "Isaksson, Martin 'How to annoy your co-authors: a Gitlab CI pipeline for LaTeX'")

## LaTeX resources

- [Wikibook on Typesetting with Latex](https://en.wikibooks.org/wiki/LaTeX)
- [Quick Start Tutorial](https://latex-tutorial.com/quick-start/)

## Information on GitLab CI for LaTeX

1. [Ajaykumar, Vipin "Continuous Integration of LaTeX projects with GitLab Pages"](https://www.vipinajayakumar.com/continuous-integration-of-latex-projects-with-gitlab-pages.html)

2. [Isaksson, Martin "How to annoy your co-authors: a Gitlab CI pipeline for LaTeX"](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/)


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.


### Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:c2a7d34721463d2c4a010776d172eb8d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:c2a7d34721463d2c4a010776d172eb8d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:c2a7d34721463d2c4a010776d172eb8d?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bkmgit/latexdemo.git
git branch -M main
git push -uf origin main
```

### Collaborate with your team

- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:c2a7d34721463d2c4a010776d172eb8d?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:c2a7d34721463d2c4a010776d172eb8d?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:c2a7d34721463d2c4a010776d172eb8d?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)


### Contributing

Please make a fork of the project, make your changes then make a merge request.

